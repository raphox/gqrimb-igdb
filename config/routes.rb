Rails.application.routes.draw do
  namespace :igdb do
    resources :games
    resources :themes
    resources :platforms
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
