json.extract! igdb_theme, :id, :igdb, :name, :slug, :created_at, :updated_at
json.url igdb_theme_url(igdb_theme, format: :json)
