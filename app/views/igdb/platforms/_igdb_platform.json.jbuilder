json.extract! igdb_platform, :id, :igdb, :name, :slug, :created_at, :updated_at
json.url igdb_platform_url(igdb_platform, format: :json)
