json.extract! igdb_game, :id, :igdb, :name, :slug, :created_at, :updated_at
json.url igdb_game_url(igdb_game, format: :json)
