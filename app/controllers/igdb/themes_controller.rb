class Igdb::ThemesController < ApplicationController
  before_action :set_igdb_theme, only: [:show, :edit, :update, :destroy]

  # GET /igdb/themes
  # GET /igdb/themes.json
  def index
    @igdb_themes = Igdb::Theme.all
  end

  # GET /igdb/themes/1
  # GET /igdb/themes/1.json
  def show
  end

  # GET /igdb/themes/new
  def new
    @igdb_theme = Igdb::Theme.new
  end

  # GET /igdb/themes/1/edit
  def edit
  end

  # POST /igdb/themes
  # POST /igdb/themes.json
  def create
    @igdb_theme = Igdb::Theme.new(igdb_theme_params)

    respond_to do |format|
      if @igdb_theme.save
        format.html { redirect_to @igdb_theme, notice: 'Theme was successfully created.' }
        format.json { render :show, status: :created, location: @igdb_theme }
      else
        format.html { render :new }
        format.json { render json: @igdb_theme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /igdb/themes/1
  # PATCH/PUT /igdb/themes/1.json
  def update
    respond_to do |format|
      if @igdb_theme.update(igdb_theme_params)
        format.html { redirect_to @igdb_theme, notice: 'Theme was successfully updated.' }
        format.json { render :show, status: :ok, location: @igdb_theme }
      else
        format.html { render :edit }
        format.json { render json: @igdb_theme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /igdb/themes/1
  # DELETE /igdb/themes/1.json
  def destroy
    @igdb_theme.destroy
    respond_to do |format|
      format.html { redirect_to igdb_themes_url, notice: 'Theme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_igdb_theme
      @igdb_theme = Igdb::Theme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def igdb_theme_params
      params.require(:igdb_theme).permit(:igdb, :name, :slug)
    end
end
