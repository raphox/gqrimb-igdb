module Igdb
  # RESTful controller to manage game resources
  class GamesController < ApplicationController
    before_action :set_igdb_game, { only: %i[show edit update destroy] }

    # GET /igdb/games
    # GET /igdb/games.json
    def index
      @igdb_games = Igdb::Game.page(params[:page])
    end

    # GET /igdb/games/1
    # GET /igdb/games/1.json
    def show; end

    # GET /igdb/games/new
    def new
      @igdb_game = Igdb::Game.new
    end

    # GET /igdb/games/1/edit
    def edit; end

    # POST /igdb/games
    # POST /igdb/games.json
    def create
      @igdb_game = Igdb::Game.new(igdb_game_params)

      respond_to do |format|
        if @igdb_game.save
          format.html { redirect_to @igdb_game, { notice: 'Game was successfully created.' } }
          format.json { render :show, { status: :created, location: @igdb_game } }
        else
          format.html { render :new }
          format.json { render({ json: @igdb_game.errors, status: :unprocessable_entity }) }
        end
      end
    end

    # PATCH/PUT /igdb/games/1
    # PATCH/PUT /igdb/games/1.json
    def update
      respond_to do |format|
        if @igdb_game.update(igdb_game_params)
          format.html { redirect_to @igdb_game, { notice: 'Game was successfully updated.' } }
          format.json { render :show, { status: :ok, location: @igdb_game } }
        else
          format.html { render :edit }
          format.json { render({ json: @igdb_game.errors, status: :unprocessable_entity }) }
        end
      end
    end

    # DELETE /igdb/games/1
    # DELETE /igdb/games/1.json
    def destroy
      @igdb_game.destroy
      respond_to do |format|
        format.html { redirect_to igdb_games_url, { notice: 'Game was successfully destroyed.' } }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_igdb_game
      @igdb_game = Igdb::Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def igdb_game_params
      params.require(:igdb_game).permit(:igdb, :name, :slug)
    end
  end
end
