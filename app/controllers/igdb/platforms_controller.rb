class Igdb::PlatformsController < ApplicationController
  before_action :set_igdb_platform, only: [:show, :edit, :update, :destroy]

  # GET /igdb/platforms
  # GET /igdb/platforms.json
  def index
    @igdb_platforms = Igdb::Platform.all
  end

  # GET /igdb/platforms/1
  # GET /igdb/platforms/1.json
  def show
  end

  # GET /igdb/platforms/new
  def new
    @igdb_platform = Igdb::Platform.new
  end

  # GET /igdb/platforms/1/edit
  def edit
  end

  # POST /igdb/platforms
  # POST /igdb/platforms.json
  def create
    @igdb_platform = Igdb::Platform.new(igdb_platform_params)

    respond_to do |format|
      if @igdb_platform.save
        format.html { redirect_to @igdb_platform, notice: 'Platform was successfully created.' }
        format.json { render :show, status: :created, location: @igdb_platform }
      else
        format.html { render :new }
        format.json { render json: @igdb_platform.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /igdb/platforms/1
  # PATCH/PUT /igdb/platforms/1.json
  def update
    respond_to do |format|
      if @igdb_platform.update(igdb_platform_params)
        format.html { redirect_to @igdb_platform, notice: 'Platform was successfully updated.' }
        format.json { render :show, status: :ok, location: @igdb_platform }
      else
        format.html { render :edit }
        format.json { render json: @igdb_platform.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /igdb/platforms/1
  # DELETE /igdb/platforms/1.json
  def destroy
    @igdb_platform.destroy
    respond_to do |format|
      format.html { redirect_to igdb_platforms_url, notice: 'Platform was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_igdb_platform
      @igdb_platform = Igdb::Platform.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def igdb_platform_params
      params.require(:igdb_platform).permit(:igdb, :name, :slug)
    end
end
