class IgdbRecord < ApplicationRecord
  self.abstract_class = true
  self.table_name_prefix = 'igdb_'
end
