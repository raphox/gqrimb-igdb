# Used to get data about games on IGDB API
module Igdb
  module API
    # Define constants about environment connections
    module ENVIRONMENTS
      ALL = {
        development: {
          api_key: 'a3e368cdfd7f580f8ae129ddf4d10d8a',
        },
        production:  {
          api_key: 'a3e368cdfd7f580f8ae129ddf4d10d8a',
        },
      }.freeze

      DEVELOPMENT, PRODUCTION = ALL.values
    end
  end
end
