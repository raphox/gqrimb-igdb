require 'optparse'
require 'igdb/api/games'
require 'igdb/api/platforms'
require 'igdb/api/themes'

ENV['RAILS_ENV'] ||= 'development'

namespace :igdb do
  task({ load: :environment }) do
    @args = ARGV[2..-1] || []
    options = OpenStruct.new({ object: :game })
    opt_parser = custom_options(options).parse(@args)

    if options.to_h.empty?
      puts opt_parser
      exit
    end

    parameters = {
      fields: options.fields.present? ? options.fields : nil,
      filters: options.filters.present? ? options.filters : nil,
    }.compact

    case options.object
    when :platform
      Igdb::API::Platforms.load(parameters)
    when :theme
      Igdb::API::Themes.load(parameters)
    when :game
      # Igdb::API::Platforms.load(parameters)
      Igdb::API::Themes.load(parameters)
      # Igdb::API::Games.load(parameters, platforms, themes)
    end

    exit
  end
end

def custom_options(options)
  OptionParser.new do |opts|
    opts.banner = 'Usage: rake igdb:load [options]'

    opts.separator ''
    opts.separator 'Specific options:'

    about = 'Fields are requested in a comma separated list'
    opts.on('-f', '--fields FIELDS', about, String) do |fields|
      options.fields = fields
    end

    about = 'Filters are parameter arrays so must be added using special keys'
    opts.on('-i', '--filters FILTER', about, String) do |filters|
      options.filters = filters
    end

    opts.on('-o', '--object OBJECT', 'resource to object on API', String) do |object|
      options.object = object.to_sym
    end

    opts.on_tail('-h', '--help', 'Show this message') do
      puts opts
      exit
    end
  end
end
