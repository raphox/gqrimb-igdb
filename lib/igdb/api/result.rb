require 'igdb'

module Igdb
  module API
    # Collection data from IGDB API
    class Result
      URL = 'https://api-endpoint.igdb.com/'.freeze
      HEADER = { 'Accept' => 'application/json' }.freeze
      HEADER_API_KEY_PARAM = 'user-key'.freeze

      attr_accessor :endpoint, :parameters, :data, :count, :next_page

      def initialize(endpoint, parameters = {})
        defaults = { limit: 50 }

        @page = 0
        @endpoint = endpoint.to_sym
        @parameters = defaults.merge(parameters)
      end

      def full_path(ids = nil)
        path_id =
          if ids.is_a?(Array)
            ids.join(',')
          else
            ids.to_s
          end

        path_base = "/#{endpoint}/"
        path_params = URI.encode_www_form(parameters)
        path = path_base + path_id
        path << "?#{path_params}" unless path_params.empty?

        path
      end

      def request(path)
        uri = URI.parse(URL)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true

        req = Net::HTTP::Get.new(path, HEADER.merge({
          HEADER_API_KEY_PARAM => ENVIRONMENTS::ALL[ENV['RAILS_ENV'].to_sym][:api_key],
        }))

        http.request(req)
      end

      def get(path = nil, ids = nil)
        path ||= full_path(ids)

        info "Conecting to '#{path}&page=#{@page}'...", :light_blue

        @data, @count, @next_page = Rails.cache.fetch("#{path}&page=#{@page}", { expires_in: 1.month }) do
          info '... without cache', :yellow

          response = request(path)

          [
            JSON.parse(response.body, { object_class: OpenStruct }),
            response.header['x-count'],
            response.header['x-next-page'],
          ]
        end
      end

      def next_page?
        @next_page.present?
      end

      def next_page!
        return false unless next_page?

        @page += 1
        get(next_page)
      end

      def empty?
        @data.empty?
      end

      def info(message, color)
        puts message.colorize(color)
      end

      def each
        result = []

        @data.each do |element|
          result << yield(element)
        end

        result
      end
    end
  end
end
