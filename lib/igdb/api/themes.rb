require 'igdb'
require 'igdb/api/result'

module Igdb
  module API
    # Themes to group Games
    class Themes
      FIELDS = %w[id name slug].freeze

      class << self
        def load(parameters)
          defaults = {
            fields: FIELDS.join(','),
            scroll: 1,
          }

          themes = []
          api = Result.new(:themes, defaults.merge(parameters))
          api.get

          loop do
            api.data.each do |item|
              attrs = {
                igdb: item.id,
                name: item.name,
                slug: item.slug,
              }

              Igdb::Theme.where(attrs).first_or_create
            end

            break unless api.next_page!
          end

          themes
        end
      end
    end
  end
end
