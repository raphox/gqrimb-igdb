require 'igdb'
require 'igdb/api/result'

module Igdb::API
  class Games
    CATEGORIES = [
      "Main game",
      "DLC / Addon",
      "Expansion",
      "Bundle",
      "Standalone expansion",
    ]
    ESRB = ["", "RP", "EC", "E", "E10+", "T", "M", "AO"]
    PEGI = [nil, 3, 7, 12, 16, 18]

    class << self
      def load(parameters, platforms, themes)
        @platforms = platforms
        @themes = themes

        defaults  = {
          fields: "id,name,slug,url,release_dates,summary,popularity,rating,rating_count,aggregated_rating,aggregated_rating_count,total_rating,total_rating_count,category,keywords,themes,genres,platforms,cover,esrb,pegi,artworks",
          scroll: 1,
          'filter[release_dates.platform][any]': "130,46,9,72,37,48,38,5,49,12"
        }

        games = []
        api = Result.new(:games, defaults.merge(parameters))
        result = api.get

        begin
          break if result.first.empty?

          result.first.each do |item|
            game = Igdb::Game.where({
              igdb: item.id,
              name: item.name,
              slug: item.slug,
            }).first_or_create do |user|
              first_release_date = Time.strptime(item.release_dates.sort_by{|t| t.date}.first.date.to_s, '%Q') rescue nil

              user.attributes = {
                url: item.url,
                first_release_date: first_release_date,
                summary: item.summary,
                popularity: item.popularity,
                rating: item.rating,
                rating_count: item.rating_count,
                aggregated_rating: item.aggregated_rating,
                aggregated_rating_count: item.aggregated_rating_count,
                total_rating: item.total_rating,
                total_rating_count: item.total_rating_count,
                category: (CATEGORIES[item.category] rescue nil),
                themes: themes?(item.themes).join(", "),
                platforms: platforms?(item.platforms).join(", "),
                cover: (item.cover.url rescue nil),
                esrb: (ESRB[item.esrb.rating] rescue nil),
                pegi: (PEGI[item.pegi.rating] rescue nil),
                artworks: (item.artworks.url rescue nil),
              }
            end

            games << item
          end
        end while result = api.next_page!

        games
      end

      def platforms?(items)
        return [] unless items

        ids = items
        @platforms.select{|item| ids.include?(item.id) }.map(&:slug)
      end

      def themes?(items)
        return [] unless items

        ids = items
        @themes.select{|item| ids.include?(item.id) }.map(&:slug)
      end
    end
  end
end
