require 'igdb'
require 'igdb/api/result'

module Igdb::API
  class Platforms
    class << self
      def load(parameters)
        defaults  = {
          fields: "id,name,slug",
          scroll: 1,
          'filter[generation][any]': "7,8"
        }

        platforms = []
        api = Result.new(:platforms, defaults.merge(parameters))
        result = api.get

        begin
          break if result.first.empty?

          result.first.each do |item|
            Igdb::Platform.where({
              igdb: item.id,
              name: item.name,
              slug: item.slug,
            }).first_or_create

            platforms << item
          end
        end while result = api.next_page!

        platforms
      end
    end
  end
end
