class CreateIgdbGames < ActiveRecord::Migration[5.2]
  def change
    create_table :igdb_games do |t|
      t.integer :igdb
      t.string :name
      t.string :slug
      t.string :url
      t.datetime :first_release_date
      t.text :summary
      t.float :popularity
      t.float :rating
      t.string :rating_count
      t.float :aggregated_rating
      t.string :aggregated_rating_count
      t.float :total_rating
      t.string :total_rating_count
      t.string :category
      t.string :themes
      t.string :platforms
      t.string :cover
      t.string :esrb
      t.string :pegi
      t.string :artworks

      t.timestamps
    end
  end
end
