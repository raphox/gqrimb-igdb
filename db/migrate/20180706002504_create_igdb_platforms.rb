class CreateIgdbPlatforms < ActiveRecord::Migration[5.2]
  def change
    create_table :igdb_platforms do |t|
      t.integer :igdb
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
