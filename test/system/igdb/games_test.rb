require "application_system_test_case"

class Igdb::GamesTest < ApplicationSystemTestCase
  setup do
    @igdb_game = igdb_games(:one)
  end

  test "visiting the index" do
    visit igdb_games_url
    assert_selector "h1", text: "Igdb/Games"
  end

  test "creating a Game" do
    visit igdb_games_url
    click_on "New Igdb/Game"

    fill_in "Igdb", with: @igdb_game.igdb
    fill_in "Name", with: @igdb_game.name
    fill_in "Slug", with: @igdb_game.slug
    click_on "Create Game"

    assert_text "Game was successfully created"
    click_on "Back"
  end

  test "updating a Game" do
    visit igdb_games_url
    click_on "Edit", match: :first

    fill_in "Igdb", with: @igdb_game.igdb
    fill_in "Name", with: @igdb_game.name
    fill_in "Slug", with: @igdb_game.slug
    click_on "Update Game"

    assert_text "Game was successfully updated"
    click_on "Back"
  end

  test "destroying a Game" do
    visit igdb_games_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Game was successfully destroyed"
  end
end
