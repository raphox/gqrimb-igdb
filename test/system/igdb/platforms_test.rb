require "application_system_test_case"

class Igdb::PlatformsTest < ApplicationSystemTestCase
  setup do
    @igdb_platform = igdb_platforms(:one)
  end

  test "visiting the index" do
    visit igdb_platforms_url
    assert_selector "h1", text: "Igdb/Platforms"
  end

  test "creating a Platform" do
    visit igdb_platforms_url
    click_on "New Igdb/Platform"

    fill_in "Igdb", with: @igdb_platform.igdb
    fill_in "Name", with: @igdb_platform.name
    fill_in "Slug", with: @igdb_platform.slug
    click_on "Create Platform"

    assert_text "Platform was successfully created"
    click_on "Back"
  end

  test "updating a Platform" do
    visit igdb_platforms_url
    click_on "Edit", match: :first

    fill_in "Igdb", with: @igdb_platform.igdb
    fill_in "Name", with: @igdb_platform.name
    fill_in "Slug", with: @igdb_platform.slug
    click_on "Update Platform"

    assert_text "Platform was successfully updated"
    click_on "Back"
  end

  test "destroying a Platform" do
    visit igdb_platforms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Platform was successfully destroyed"
  end
end
