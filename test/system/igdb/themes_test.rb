require "application_system_test_case"

class Igdb::ThemesTest < ApplicationSystemTestCase
  setup do
    @igdb_theme = igdb_themes(:one)
  end

  test "visiting the index" do
    visit igdb_themes_url
    assert_selector "h1", text: "Igdb/Themes"
  end

  test "creating a Theme" do
    visit igdb_themes_url
    click_on "New Igdb/Theme"

    fill_in "Igdb", with: @igdb_theme.igdb
    fill_in "Name", with: @igdb_theme.name
    fill_in "Slug", with: @igdb_theme.slug
    click_on "Create Theme"

    assert_text "Theme was successfully created"
    click_on "Back"
  end

  test "updating a Theme" do
    visit igdb_themes_url
    click_on "Edit", match: :first

    fill_in "Igdb", with: @igdb_theme.igdb
    fill_in "Name", with: @igdb_theme.name
    fill_in "Slug", with: @igdb_theme.slug
    click_on "Update Theme"

    assert_text "Theme was successfully updated"
    click_on "Back"
  end

  test "destroying a Theme" do
    visit igdb_themes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Theme was successfully destroyed"
  end
end
