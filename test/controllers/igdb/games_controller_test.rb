require 'test_helper'

class Igdb::GamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @igdb_game = igdb_games(:one)
  end

  test "should get index" do
    get igdb_games_url
    assert_response :success
  end

  test "should get new" do
    get new_igdb_game_url
    assert_response :success
  end

  test "should create igdb_game" do
    assert_difference('Igdb::Game.count') do
      post igdb_games_url, params: { igdb_game: { igdb: @igdb_game.igdb, name: @igdb_game.name, slug: @igdb_game.slug } }
    end

    assert_redirected_to igdb_game_url(Igdb::Game.last)
  end

  test "should show igdb_game" do
    get igdb_game_url(@igdb_game)
    assert_response :success
  end

  test "should get edit" do
    get edit_igdb_game_url(@igdb_game)
    assert_response :success
  end

  test "should update igdb_game" do
    patch igdb_game_url(@igdb_game), params: { igdb_game: { igdb: @igdb_game.igdb, name: @igdb_game.name, slug: @igdb_game.slug } }
    assert_redirected_to igdb_game_url(@igdb_game)
  end

  test "should destroy igdb_game" do
    assert_difference('Igdb::Game.count', -1) do
      delete igdb_game_url(@igdb_game)
    end

    assert_redirected_to igdb_games_url
  end
end
