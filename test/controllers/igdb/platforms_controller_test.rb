require 'test_helper'

class Igdb::PlatformsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @igdb_platform = igdb_platforms(:one)
  end

  test "should get index" do
    get igdb_platforms_url
    assert_response :success
  end

  test "should get new" do
    get new_igdb_platform_url
    assert_response :success
  end

  test "should create igdb_platform" do
    assert_difference('Igdb::Platform.count') do
      post igdb_platforms_url, params: { igdb_platform: { igdb: @igdb_platform.igdb, name: @igdb_platform.name, slug: @igdb_platform.slug } }
    end

    assert_redirected_to igdb_platform_url(Igdb::Platform.last)
  end

  test "should show igdb_platform" do
    get igdb_platform_url(@igdb_platform)
    assert_response :success
  end

  test "should get edit" do
    get edit_igdb_platform_url(@igdb_platform)
    assert_response :success
  end

  test "should update igdb_platform" do
    patch igdb_platform_url(@igdb_platform), params: { igdb_platform: { igdb: @igdb_platform.igdb, name: @igdb_platform.name, slug: @igdb_platform.slug } }
    assert_redirected_to igdb_platform_url(@igdb_platform)
  end

  test "should destroy igdb_platform" do
    assert_difference('Igdb::Platform.count', -1) do
      delete igdb_platform_url(@igdb_platform)
    end

    assert_redirected_to igdb_platforms_url
  end
end
