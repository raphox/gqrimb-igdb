require 'test_helper'

class Igdb::ThemesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @igdb_theme = igdb_themes(:one)
  end

  test "should get index" do
    get igdb_themes_url
    assert_response :success
  end

  test "should get new" do
    get new_igdb_theme_url
    assert_response :success
  end

  test "should create igdb_theme" do
    assert_difference('Igdb::Theme.count') do
      post igdb_themes_url, params: { igdb_theme: { igdb: @igdb_theme.igdb, name: @igdb_theme.name, slug: @igdb_theme.slug } }
    end

    assert_redirected_to igdb_theme_url(Igdb::Theme.last)
  end

  test "should show igdb_theme" do
    get igdb_theme_url(@igdb_theme)
    assert_response :success
  end

  test "should get edit" do
    get edit_igdb_theme_url(@igdb_theme)
    assert_response :success
  end

  test "should update igdb_theme" do
    patch igdb_theme_url(@igdb_theme), params: { igdb_theme: { igdb: @igdb_theme.igdb, name: @igdb_theme.name, slug: @igdb_theme.slug } }
    assert_redirected_to igdb_theme_url(@igdb_theme)
  end

  test "should destroy igdb_theme" do
    assert_difference('Igdb::Theme.count', -1) do
      delete igdb_theme_url(@igdb_theme)
    end

    assert_redirected_to igdb_themes_url
  end
end
